package pl.exaco.internship.android.weatherdemo.service

interface IServiceFactory {

    fun getCitiesManager(): ICitiesManager

    fun getWeatherManager(): IWeatherManager
}