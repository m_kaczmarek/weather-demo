package pl.exaco.internship.android.weatherdemo.service.api

import pl.exaco.internship.android.weatherdemo.model.CitiesWeather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET(value = "group")
    fun forSeveralCities(@Query("id") cityIds: String): Call<CitiesWeather>

}