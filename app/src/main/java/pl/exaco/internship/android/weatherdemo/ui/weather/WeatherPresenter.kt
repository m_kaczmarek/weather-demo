package pl.exaco.internship.android.weatherdemo.ui.weather

import pl.exaco.internship.android.weatherdemo.model.CityWeather
import pl.exaco.internship.android.weatherdemo.service.ICitiesManager
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory
import pl.exaco.internship.android.weatherdemo.service.IWeatherManager
import pl.exaco.internship.android.weatherdemo.service.RequestCallback


class WeatherPresenter(
        private val view: WeatherContract.View,
        serviceFactory: IServiceFactory
) : WeatherContract.Presenter {

    private val citiesManager: ICitiesManager = serviceFactory.getCitiesManager()
    private val weatherManager: IWeatherManager = serviceFactory.getWeatherManager()

    override fun loadSavedCities() = citiesManager.getSavedCities()

    override fun getWeatherForSavedCities() {
        weatherManager.getWeatherForCities(loadSavedCities(), object : RequestCallback<List<CityWeather>> {
            override fun onSuccess(data: List<CityWeather>) {
                view.onDataLoaded(data)
            }

            override fun onError(throwable: Throwable) {
                view.onDataLoaded(listOf())
            }
        })
    }
}