package pl.exaco.internship.android.weatherdemo.service.impl

import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import pl.exaco.internship.android.weatherdemo.service.ICitiesManager
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory
import pl.exaco.internship.android.weatherdemo.service.IWeatherManager


@EBean(scope = EBean.Scope.Singleton)
class ServiceFactory : IServiceFactory {

    @Bean(value = CitiesManager::class)
    lateinit var iCitiesManager: ICitiesManager

    @Bean(value = WeatherManager::class)
    lateinit var iWeatherManager: IWeatherManager

    override fun getCitiesManager() = iCitiesManager

    override fun getWeatherManager(): IWeatherManager = iWeatherManager
}