package pl.exaco.internship.android.weatherdemo.model

import com.google.gson.annotations.SerializedName


data class CitiesWeather(
        @SerializedName("cnt") val count: Int,
        @SerializedName("list") val list: List<CityWeather>
)