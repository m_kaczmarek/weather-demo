package pl.exaco.internship.android.weatherdemo.ui.city

import pl.exaco.internship.android.weatherdemo.model.City

interface CityContract {
    interface View {
        fun onCitiesLoaded(cities: List<City>)
    }

    interface Presenter {
        fun loadCities()

        fun addCity(city: City)
    }
}