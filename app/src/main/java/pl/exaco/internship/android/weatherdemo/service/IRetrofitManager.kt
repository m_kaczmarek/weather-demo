package pl.exaco.internship.android.weatherdemo.service

import retrofit2.Retrofit

interface IRetrofitManager {

    fun getRetrofit(): Retrofit
}