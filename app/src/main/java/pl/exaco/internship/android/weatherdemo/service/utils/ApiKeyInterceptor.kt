package pl.exaco.internship.android.weatherdemo.service.utils

import okhttp3.Interceptor
import okhttp3.Response


class ApiKeyInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain
                .request()
                .url()
                .newBuilder()
                .addQueryParameter(APP_ID, API_KEY)
                .addQueryParameter(UNITS, UNITS_VALUE)
                .addQueryParameter(LANGUAGE, LANGUAGE_VAL)
                .build()
        val request = chain
                .request()
                .newBuilder()
                .url(url)
                .build()
        return chain.proceed(request)
    }

    companion object {
        const val UNITS = "units"
        const val UNITS_VALUE = "metric"
        const val APP_ID = "APPID"
        const val API_KEY = "ffbf64c74874f4e29e9b1252ef6a0a90"
        const val LANGUAGE = "lang"
        const val LANGUAGE_VAL = "pl"
    }
}