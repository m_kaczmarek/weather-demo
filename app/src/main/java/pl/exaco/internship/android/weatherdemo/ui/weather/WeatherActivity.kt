package pl.exaco.internship.android.weatherdemo.ui.weather

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import org.androidannotations.annotations.*
import pl.exaco.internship.android.weatherdemo.R
import pl.exaco.internship.android.weatherdemo.databinding.ActivityCitiesBinding
import pl.exaco.internship.android.weatherdemo.model.CityWeather
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory
import pl.exaco.internship.android.weatherdemo.service.impl.ServiceFactory
import pl.exaco.internship.android.weatherdemo.ui.city.CityActivity_

@DataBound
@EActivity(R.layout.activity_weather)
@OptionsMenu(R.menu.menu_weather)
class WeatherActivity : AppCompatActivity(), WeatherContract.View {

    @Bean(value = ServiceFactory::class)
    lateinit var serviceFactory: IServiceFactory

    @BindingObject
    lateinit var binding: ActivityCitiesBinding

    lateinit var presenter: WeatherContract.Presenter
    lateinit var adapter: WeatherAdapter

    @AfterViews
    fun viewCreated() {
        presenter = WeatherPresenter(this, serviceFactory)
        adapter = WeatherAdapter(this)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter
        loadData()
    }

    fun loadData() {
        binding.recyclerView.visibility = View.GONE
        binding.noData.visibility = View.GONE
        binding.progressBar.visibility = View.VISIBLE
        presenter.getWeatherForSavedCities()
    }

    override fun onDataLoaded(cities: List<CityWeather>) {
        binding.progressBar.visibility = View.GONE
        if (cities.isEmpty()) {
            binding.noData.visibility = View.VISIBLE
        } else {
            binding.recyclerView.visibility = View.VISIBLE
            adapter.setItems(cities)
        }
    }

    @OptionsItem(R.id.add)
    fun addCity() {
        CityActivity_
                .intent(this)
                .startForResult(ADD_CITY_REQUEST)
    }

    @OnActivityResult(value = ADD_CITY_REQUEST)
    fun onResult() {
        loadData()
    }

    companion object {
        const val ADD_CITY_REQUEST = 15111
    }
}
