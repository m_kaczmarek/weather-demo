package pl.exaco.internship.android.weatherdemo.ui.city

import pl.exaco.internship.android.weatherdemo.model.City
import pl.exaco.internship.android.weatherdemo.service.ICitiesManager
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory
import pl.exaco.internship.android.weatherdemo.service.RequestCallback

class CityPresenter(
        private val view: CityContract.View,
        serviceFactory: IServiceFactory
) : CityContract.Presenter {

    private val citiesManager: ICitiesManager = serviceFactory.getCitiesManager()


    override fun loadCities() {
        citiesManager.loadCities(object : RequestCallback<List<City>> {
            override fun onSuccess(data: List<City>) {
                view.onCitiesLoaded(data)
            }

            override fun onError(throwable: Throwable) {
                // ignore
            }
        })
    }

    override fun addCity(city: City) {
        citiesManager.addCity(city)
    }
}