package pl.exaco.internship.android.weatherdemo.service

interface RequestCallback<T> {

    fun onSuccess(data: T)

    fun onError(throwable: Throwable)
}