package pl.exaco.internship.android.weatherdemo.service.impl

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.androidannotations.annotations.AfterInject
import org.androidannotations.annotations.EBean
import pl.exaco.internship.android.weatherdemo.service.IRetrofitManager
import pl.exaco.internship.android.weatherdemo.service.utils.ApiKeyInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@EBean(scope = EBean.Scope.Singleton)
class RetrofitManager : IRetrofitManager {

    lateinit var retrofitClient: Retrofit

    @AfterInject
    fun init() {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttp = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(ApiKeyInterceptor())
                .build()
        retrofitClient = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    override fun getRetrofit(): Retrofit = retrofitClient

    companion object {
        const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
    }
}