package pl.exaco.internship.android.weatherdemo.service.impl

import org.androidannotations.annotations.AfterInject
import org.androidannotations.annotations.EBean
import pl.exaco.internship.android.weatherdemo.model.CitiesWeather
import pl.exaco.internship.android.weatherdemo.model.City
import pl.exaco.internship.android.weatherdemo.model.CityWeather
import pl.exaco.internship.android.weatherdemo.service.IWeatherManager
import pl.exaco.internship.android.weatherdemo.service.RequestCallback
import pl.exaco.internship.android.weatherdemo.service.api.WeatherApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


@EBean(scope = EBean.Scope.Singleton)
class WeatherManager : IWeatherManager, BaseManager() {

    lateinit var api: WeatherApi

    @AfterInject
    fun initService() {
        api = retrofit.create(WeatherApi::class.java)
    }

    override fun getWeatherForCities(cities: List<City>, callback: RequestCallback<List<CityWeather>>) {
        api.forSeveralCities(cities.map { it.id }.joinToString(",")).enqueue(object : Callback<CitiesWeather> {
            override fun onResponse(call: Call<CitiesWeather>, response: Response<CitiesWeather>) {
                if (response.isSuccessful) {
                    callback.onSuccess(response.body()?.list ?: listOf())
                } else {
                    callback.onError(Exception(response.errorBody()!!.string()))
                }
            }

            override fun onFailure(call: Call<CitiesWeather>, throwable: Throwable) {
                callback.onError(throwable)
            }

        })
    }
}