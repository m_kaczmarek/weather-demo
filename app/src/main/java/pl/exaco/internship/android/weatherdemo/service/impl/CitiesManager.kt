package pl.exaco.internship.android.weatherdemo.service.impl

import android.os.AsyncTask
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.androidannotations.annotations.App
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import pl.exaco.internship.android.weatherdemo.R
import pl.exaco.internship.android.weatherdemo.WeatherApplication
import pl.exaco.internship.android.weatherdemo.db.IPrefDataStore
import pl.exaco.internship.android.weatherdemo.db.impl.PrefDataStore
import pl.exaco.internship.android.weatherdemo.model.City
import pl.exaco.internship.android.weatherdemo.service.ICitiesManager
import pl.exaco.internship.android.weatherdemo.service.RequestCallback
import java.io.BufferedReader
import java.io.InputStreamReader


@EBean(scope = EBean.Scope.Singleton)
class CitiesManager : BaseManager(), ICitiesManager {

    @Bean(value = PrefDataStore::class)
    lateinit var prefDataStore: IPrefDataStore

    @App
    lateinit var app: WeatherApplication

    override fun getSavedCities(): List<City> = prefDataStore.getSavedCities()

    override fun loadCities(callback: RequestCallback<List<City>>) {
        AsyncTask.execute {
            try {
                val raw = app.resources.openRawResource(R.raw.cities)
                val rd = BufferedReader(InputStreamReader(raw))
                val listType = TypeToken.getParameterized(List::class.java, City::class.java).type
                callback.onSuccess(Gson().fromJson(rd, listType))
            } catch (e: Exception) {
                callback.onError(e)
            }
        }
    }

    override fun addCity(city: City) {
        prefDataStore.addNewCity(city)
    }
}