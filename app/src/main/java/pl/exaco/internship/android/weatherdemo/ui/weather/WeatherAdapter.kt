package pl.exaco.internship.android.weatherdemo.ui.weather

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.exaco.internship.android.weatherdemo.R
import pl.exaco.internship.android.weatherdemo.databinding.ItemWeatherBinding
import pl.exaco.internship.android.weatherdemo.model.CityWeather

class WeatherAdapter(
        private val context: Context
) : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    private val items = mutableListOf<CityWeather>()

    fun setItems(items: List<CityWeather>) {
        this.items.clear()
        this.items.addAll(items)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_weather, parent, false)
        return WeatherViewHolder(view)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        if (position < itemCount) {
            holder.bindData(items[position])
        }
    }

    class WeatherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val binding: ItemWeatherBinding = DataBindingUtil.bind(itemView)!!

        fun bindData(data: CityWeather) {
            binding.cityWeather = data
            binding.setDescription(data.cityWeather[0])
        }
    }
}
