package pl.exaco.internship.android.weatherdemo.ui.util

import android.databinding.BindingAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter(value = ["imageUrl"])
fun ImageView.setImageUrl(iconName: String?) {
    val url = "http://openweathermap.org/img/w/${iconName.orEmpty()}.png"
    Glide
            .with(context)
            .load(url)
            .into(this)
}


@BindingAdapter(value = ["dateTime"])
fun TextView.setDateTime(time: Long?) {
    val formatter = SimpleDateFormat("HH:mm", Locale.getDefault())
    var date = System.currentTimeMillis()
    if (time != null) {
        date = time * 1000
    }
    this.text = formatter.format(Date(date))
}