package pl.exaco.internship.android.weatherdemo.db

import pl.exaco.internship.android.weatherdemo.model.City

interface IPrefDataStore {

    fun getSavedCities(): List<City>

    fun addNewCity(city: City)

}