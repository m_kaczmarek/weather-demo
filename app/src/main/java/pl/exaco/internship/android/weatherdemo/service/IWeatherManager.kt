package pl.exaco.internship.android.weatherdemo.service

import pl.exaco.internship.android.weatherdemo.model.City
import pl.exaco.internship.android.weatherdemo.model.CityWeather

interface IWeatherManager {

    fun getWeatherForCities(cities: List<City>, callback: RequestCallback<List<CityWeather>>)
}