package pl.exaco.internship.android.weatherdemo.service.impl

import org.androidannotations.annotations.AfterInject
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean
import retrofit2.Retrofit


@EBean(scope = EBean.Scope.Singleton)
abstract class BaseManager {

    @Bean
    lateinit var retrofitManager: RetrofitManager

    lateinit var retrofit: Retrofit

    @AfterInject
    fun init() {
        retrofit = retrofitManager.getRetrofit()
    }
}