package pl.exaco.internship.android.weatherdemo.ui.city

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import pl.exaco.internship.android.weatherdemo.R
import pl.exaco.internship.android.weatherdemo.databinding.ItemSimpleCityBinding
import pl.exaco.internship.android.weatherdemo.model.City

class CityAdapter(
        private val context: Context,
        private val listener: AdapterListener
) : RecyclerView.Adapter<CityAdapter.SimpleCityViewHolder>(), Filterable {

    private val filter = CityFilter(this)
    private val items = mutableListOf<City>()
    private val filtered = mutableListOf<City>()

    fun setItems(items: List<City>) {
        this.items.clear()
        this.items.addAll(items)
        this.filtered.clear()
        this.filtered.addAll(items)
        this.notifyDataSetChanged()
    }

    fun setFilteredResult(items: List<City>) {
        filtered.clear()
        filtered.addAll(items)
        notifyDataSetChanged()
        listener.filterResult(itemCount)
    }

    override fun getItemCount(): Int = filtered.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleCityViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_city, parent, false)
        return SimpleCityViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SimpleCityViewHolder, position: Int) {
        if (position < itemCount) {
            val city = filtered[position]
            holder.bindData(city)
            holder.binding.root.setOnClickListener { listener.onCitySelected(city) }
        }
    }

    override fun getFilter(): Filter = filter

    class CityFilter(
            private val adapter: CityAdapter
    ) : Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {

            val result = FilterResults()
            val items = adapter.items.filter { it.name.contains(constraint, true) || it.country.contains(constraint, true) }
            result.values = items
            result.count = items.size
            return result
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            adapter.setFilteredResult(results.values as List<City>)
        }
    }

    class SimpleCityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val binding: ItemSimpleCityBinding = DataBindingUtil.bind(itemView)!!

        fun bindData(data: City) {
            binding.city = data
        }
    }

    interface AdapterListener {
        fun onCitySelected(city: City)

        fun filterResult(count: Int)
    }
}