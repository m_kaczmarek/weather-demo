package pl.exaco.internship.android.weatherdemo.db.impl

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.androidannotations.annotations.App
import org.androidannotations.annotations.EBean
import pl.exaco.internship.android.weatherdemo.WeatherApplication
import pl.exaco.internship.android.weatherdemo.db.IPrefDataStore
import pl.exaco.internship.android.weatherdemo.model.City


@EBean(scope = EBean.Scope.Singleton)
class PrefDataStore : IPrefDataStore {

    companion object {
        const val CITIES = "CITIES"
        const val MAX_CITIES = 5
        const val FIRST_ITEM = 0
    }

    @App
    lateinit var application: WeatherApplication

    var cities: MutableList<City> = mutableListOf()

    override fun getSavedCities(): List<City> {
        if (cities.isEmpty()) {
            cities = getList(CITIES, City::class.java)
        }
        return cities
    }

    override fun addNewCity(city: City) {
        if (cities.size == MAX_CITIES) {
            cities.removeAt(FIRST_ITEM)
        }
        cities.add(city)
        setList(cities, CITIES)
    }

    private fun <T> setList(list: List<T>, prefKey: String) {
        val listString = Gson().toJson(list)
        getPreferences().edit().putString(prefKey, listString).apply()
    }

    private fun <T> getList(prefKey: String, listObjectClass: Class<T>): MutableList<T> {
        val listString = getPreferences().getString(prefKey, null).orEmpty()
        val listType = TypeToken.getParameterized(List::class.java, listObjectClass).type
        return Gson().fromJson(listString, listType) ?: mutableListOf()
    }

    private fun getPreferences(): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)
}