package pl.exaco.internship.android.weatherdemo.ui.weather

import pl.exaco.internship.android.weatherdemo.model.City
import pl.exaco.internship.android.weatherdemo.model.CityWeather

interface WeatherContract {

    interface Presenter {
        fun loadSavedCities(): List<City>

        fun getWeatherForSavedCities()
    }

    interface View {
        fun onDataLoaded(cityWeather: List<CityWeather>)
    }
}