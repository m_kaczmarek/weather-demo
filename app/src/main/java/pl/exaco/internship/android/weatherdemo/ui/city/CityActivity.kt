package pl.exaco.internship.android.weatherdemo.ui.city


import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.SearchView
import org.androidannotations.annotations.*
import pl.exaco.internship.android.weatherdemo.R
import pl.exaco.internship.android.weatherdemo.databinding.ActivityFindCityBinding
import pl.exaco.internship.android.weatherdemo.model.City
import pl.exaco.internship.android.weatherdemo.service.IServiceFactory
import pl.exaco.internship.android.weatherdemo.service.impl.ServiceFactory

@DataBound
@EActivity(R.layout.activity_city)
class CityActivity : AppCompatActivity(), CityContract.View {

    @BindingObject
    lateinit var binding: ActivityFindCityBinding

    @Bean(ServiceFactory::class)
    lateinit var serviceFactory: IServiceFactory

    lateinit var presenter: CityContract.Presenter
    lateinit var adapter: CityAdapter

    @AfterViews
    fun onViewCreated() {
        presenter = CityPresenter(this, serviceFactory)
        adapter = CityAdapter(this, object : CityAdapter.AdapterListener {
            override fun onCitySelected(city: City) {
                presenter.addCity(city)
                setResult(Activity.RESULT_OK)
                finish()
            }

            override fun filterResult(count: Int) {
                binding.hasItems = count > 0
                if (count > 0) {
                    binding.noData.visibility = View.GONE
                } else {
                    binding.noData.visibility = View.VISIBLE
                }
            }
        })
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.setHasFixedSize(false)
        binding.recyclerView.adapter = adapter
        binding.search.onActionViewExpanded()
        binding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener, android.support.v7.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = true

            override fun onQueryTextChange(newText: String): Boolean {
                adapter.filter.filter(newText)
                return true
            }

        })
        loadData()
    }

    fun loadData() {
        binding.progressBar.visibility = View.VISIBLE
        binding.recyclerView.visibility = View.GONE
        binding.search.visibility = View.GONE
        binding.noData.visibility = View.GONE
        presenter.loadCities()
    }

    @UiThread
    override fun onCitiesLoaded(cities: List<City>) {
        binding.progressBar.visibility = View.GONE
        binding.search.visibility = View.VISIBLE
        if (cities.isEmpty()) {
            binding.noData.visibility = View.VISIBLE
        }
        binding.hasItems = !cities.isEmpty()
        adapter.setItems(cities)
    }
}
