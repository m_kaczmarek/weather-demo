package pl.exaco.internship.android.weatherdemo.service

import pl.exaco.internship.android.weatherdemo.model.City

interface ICitiesManager {

    fun getSavedCities(): List<City>

    fun loadCities(callback: RequestCallback<List<City>>)

    fun addCity(city: City)
}